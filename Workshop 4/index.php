<?php

$csvroute = $argv[1];
$txtroute = $argv[2];
$data = array();
$texto = "nombre apellido lives in lugar and his phonenumber is numero ";
if (($filecsv = fopen($csvroute, "r")) !== FALSE) {
    $titles = fgetcsv($filecsv, 1000, ",");
    $title_len = count($titles);
    while (($information = fgetcsv($filecsv, 1000, ",")) !== FALSE) {
        for ($pos = 0; $pos < $title_len; $pos++) {
            $record[$titles[$pos]] = $information[$pos];
        }
        $records[] = $record;
    }
    fclose($filecsv);
    //Como se creo un arreglo asociativo se lee como si fuera una matriz
    $filetxt = fopen($txtroute,"w"); 
   for ($i = 0; $i < count($records); $i++) {
        for ($pos = 0; $pos < $title_len; $pos++) {
            if($pos == 0)
            {
                $rest = str_replace("nombre",$records[$i][$titles[$pos]],$texto);
                $texto = $rest;   
            }
            if($pos == 1)
            {
                $rest = str_replace("apellido",$records[$i][$titles[$pos]],$texto);
                $texto = $rest; 
            }
            if($pos == 2)
            {
                $rest = str_replace("numero",$records[$i][$titles[$pos]],$texto);
                $texto = $rest;    
            }
            if($pos == 3)
            {
                $rest = str_replace("lugar",$records[$i][$titles[$pos]],$texto);
                $texto = $rest;
                fwrite($filetxt, $texto . PHP_EOL);    
            } 
        }
        $texto = "nombre apellido lives in lugar and his phonenumber is numero ";
    } 
    fclose($filetxt);
}

?>