<?php

require 'persona.php';

if(empty($argv))
{
    echo "You need to write the next parameters:\n
    1. Id\n
    2. Name\n
    3. Lastname\n
    4. Email\n
    5. Here you have to specify the type of action that you want to realize\n
    (U for update a record, C for create a new record or D for delete a record).";
}
else{
    if(empty($argv[5]))
    {
        echo"You need to specify a valid action (U for update a record, C for create a new record or D for delete a record).";
    }
    else
    {
     switch($argv[5])
        {
            case "U":
                $student = new Person($argv[1],$argv[2],$argv[3],$argv[4]);
                $student ->update();
                break;
            case "C":
                $student = new Person($argv[1],$argv[2],$argv[3],$argv[4]);
                $student ->insert();
                break;
            case "D":
                $student = new Person($argv[1],$argv[2],$argv[3],$argv[4]);
                $student ->delete();
                break;
            default:
                echo "You need to specify a valid action (U for update a record, C for create a new record or D for delete a record).";
                break;

        }
    }
}