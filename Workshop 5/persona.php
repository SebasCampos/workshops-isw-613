<?php

require 'DataAccess.php';
class Person{
    
    public $cedula;
    public $nombre;
    public $apellido;
    public $email;


    function __construct($cedula,$nombre,$apellido,$email)
    {
        $this->cedula = $cedula;
        $this->nombre = $nombre;
        $this->apellido = $apellido;
        $this->email = $email;
    }
    /*
    This function is used for insert a new record in the database
    */
    function insert()
    {
        $con = new Conect("127.0.0.1","root","1234","pruebas");
        $open_con = $con->get_connection();
        $sql = "INSERT INTO estudiantes (id, first_name, last_name, email) VALUES ($this->cedula, '$this->nombre', '$this->apellido', '$this->email')";
        if (mysqli_query($open_con, $sql)) {
            echo "New record created successfully.";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($open_con);
        }
        mysqli_close($open_con);
    }

    /*
    This function is used for update the records in the database
    */
    function update()
    {
        $con = new Conect("127.0.0.1","root","1234","pruebas");
        $open_con = $con->get_connection();
        $sql = "UPDATE estudiantes SET email='$this->email' WHERE id = '$this->cedula' ";
        if (mysqli_query($open_con, $sql)) {
            echo "The record was updated successfully.";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($open_con);
        }
        mysqli_close($open_con);
    }

     /*
    This function is used for delete the records in the database
    */
    function delete()
    {
        $con = new Conect("127.0.0.1","root","1234","pruebas");
        $open_con = $con->get_connection();
        $sql = "DELETE FROM estudiantes WHERE id = '$this->cedula' ";
        if (mysqli_query($open_con, $sql)) {
            echo "The record was deleted successfully.";
        } else {
            echo "Error: " . $sql . "<br>" . mysqli_error($open_con);
        }
        mysqli_close($open_con);
    }

    function to_string() {
        return "{$this->cedula} - {$this->nombre} - {$this->apellido} - {$this->email} ". PHP_EOL;
     }
}